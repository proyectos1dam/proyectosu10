package es.cipfpbatoi.modelo.entidades;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Tarea {

	private int codigo;
	private String nombreUsuario;
	private String descripcion;
	private LocalDateTime creadoEn;
	private LocalDateTime vencimiento;
	private boolean realizada;
	private Prioridad prioridad;
	public enum Prioridad {
		ALTA, MEDIA, BAJA
	}

	public Tarea(int codigo, String nombreUsuario, String descripcion, LocalDateTime vencimiento, Prioridad prioridad, boolean realizada) {
		this.codigo = codigo;
		this.nombreUsuario = nombreUsuario;
		this.descripcion = descripcion;
		this.creadoEn = LocalDateTime.now();
		this.vencimiento = vencimiento;
		this.prioridad = prioridad;
		this.realizada = realizada;
	}

	public int getCodigo() {
		return codigo;
	}	

	public String getNombreUsuario() {
		return nombreUsuario;
	}	

	public String getDescripcion() {
		return descripcion;
	}

	public LocalDateTime getCreadoEn() {
		return creadoEn;
	}

	public LocalDateTime getVencimiento() {
		return vencimiento;
	}
	
	public Prioridad getPrioridad() {
		return prioridad;
	}

	public boolean isRealizada() {
		return realizada;
	}

	public void setRealizada(boolean realizada) {
		this.realizada = realizada;
	}

	@Override
	public int hashCode() {
		return Objects.hash(codigo);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tarea other = (Tarea) obj;
		return codigo == other.codigo;
	}

	@Override
	public String toString() {
		DateTimeFormatter formatterDateTime = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		
		return "Tarea " + codigo + " de " + nombreUsuario + " (" + descripcion + ")" 
				+"<br> Creada " + creadoEn.format(formatterDateTime)
				+ "<br> Vencimiento " + vencimiento.format(formatterDateTime)
				+ "<br> "  + (realizada?"Realizada":"No realizada") + " - Prioridad " + prioridad.toString();
	}
	
	

}
